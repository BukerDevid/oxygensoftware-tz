#define WIN32_LEAN_AND_MEAN

#include <windows.h>
#include <stdio.h>
#include <exception>


#define _DLLAPI extern "C" __declspec(dllexport)


_DLLAPI int __stdcall extractWindowsVersion(int *arr, char *in_str){
    //Объявляем переменные
    DWORD dwVersion = 0;
    DWORD dwMajorVersion = 0;
    DWORD dwMinorVersion = 0;
    DWORD dwBuild = 0;
    try
    {
        //Получаем версию Windows
        dwVersion = GetVersion();
        //Производим выборку из старшего и младшего байта версии Windows
        dwMajorVersion = (DWORD)(LOBYTE(LOWORD(dwVersion)));
        dwMinorVersion = (DWORD)(HIBYTE(LOWORD(dwVersion)));
        //Получаем номер сборки
        if (dwVersion < 0x80000000)
            dwBuild = (DWORD)(HIWORD(dwVersion));
        //Заворачиваем ответ в массив
        arr[0]=dwMajorVersion;
        arr[1]=dwMinorVersion;
        arr[2]=dwBuild;
        //Формируем строку
        sprintf(in_str,"%d.%d.%d",
                dwMajorVersion,
                dwMinorVersion,
                dwBuild);
        return 0;
    }
    catch(const std::exception& e)
    {
        throw e.what();
    }
}